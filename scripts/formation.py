#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Vector3Stamped
from mrs_msgs.srv import Vec4Request,Vec4

def handle_uav_distcmd(msg,uavname):
    global gotoPos,lastCommandTime,SetGoal,Pose
    try:
        if (gotoPos==None):
            gotoPos = rospy.ServiceProxy('/%s/control_manager/goto_relative' % uavname, Vec4)

        if (SetGoal==None):
            SetGoal = rospy.ServiceProxy('/%s/local_planner_node/SetGoal' % uavname, Vec4)
        
        elapsedTime = (rospy.Time.now().to_nsec() - lastCommandTime) / 10.0 ** 9
        if elapsedTime>=0.5:
            # print elapsedTime
            lastCommandTime = rospy.Time.now().to_nsec()

            # resp = gotoPos(position)
            if Pose != None:
                position = Vec4Request()
                position.goal[0] = Pose.position.x + msg.vector.x 
                position.goal[1] = Pose.position.y + msg.vector.y
                position.goal[2] = Pose.position.z + msg.vector.z
                resp = SetGoal(position)
        # print position ,resp
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e )

    # rate.sleep()


def handle_uav_pose(msg):
    global Pose
    Pose = msg.pose
    
if __name__ == '__main__':
    rospy.init_node('uav_formation_control')
    uavname = rospy.get_param('~uavName')
    # uavname = "uav1"
    rospy.wait_for_service('/%s/control_manager/goto_relative' % uavname)
    rospy.wait_for_service('/%s/local_planner_node/SetGoal' % uavname)
    newName = uavname
    gotoPos = None 
    SetGoal = None 
    Pose = None 
    lastCommandTime = 0
    if uavname =="uav1":
        newName = "SQ01s"
    
    if uavname =="uav2":
        newName = "SQ02s"
    
    if uavname =="uav3":
        newName = "SQ03s"
    
    if uavname =="uav4":
        newName = "SQ04s"
    
    rate = rospy.Rate(10) # 1hz

    rospy.Subscriber('/%s/distcmd' % newName,
                     Vector3Stamped,
                     handle_uav_distcmd,uavname)

    rospy.Subscriber('/%s/world' % newName,
                     PoseStamped,
                     handle_uav_pose)

    while not rospy.is_shutdown():
        rospy.spin()
