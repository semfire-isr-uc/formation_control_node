#!/usr/bin/env python

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from snapstack_msgs.msg import State

def handle_uav_odom(msg,uavname):
    global ex,ey,ez,RandomErrorCalculated,lastCommandTime
    msgPose = PoseStamped()
    msgPose.pose = msg.pose.pose
    # rx,ry,rz = np.random.uniform(low=-0.5,high=0.5,size=(3,))
    
    # print rospy.Time.now().secs, rospy.Time.now().secs % 1000

    elapsedTime = (rospy.Time.now().to_nsec() - lastCommandTime) / 10.0 ** 9
    if elapsedTime>=0.5 and not RandomErrorCalculated:
        # ex,ey,ez = np.random.uniform(-0.5,0.5,(3,))       
        RandomErrorCalculated = True 

    # print ex,ey,ez,elapsedTime
    
    if RandomErrorCalculated and elapsedTime>=5.0:
        lastCommandTime = rospy.Time.now().to_nsec()
        msgPose.pose.position.x+=ex
        msgPose.pose.position.y+=ey
        msgPose.pose.position.z+=ez
        RandomErrorCalculated = False

    msgPose.header.stamp = rospy.Time.now()
    msgPose.header.frame_id = "world" # msg.header.frame_id # '%s/gps_origin' % uavname# 
    pub.publish(msgPose)
    # rate.sleep()

def handle_uav_odom_local(msg,uavname):
    global ex,ey,ez,RandomErrorDone
    msgState = State()
    msgState.pos = msg.pose.pose.position
    msgState.quat = msg.pose.pose.orientation
    
    msgState.pos.x+=ex
    msgState.pos.y+=ey
    msgState.pos.z+=ez
    msgState.header.stamp = rospy.Time.now() #msg.header.stamp
    msgState.header.frame_id = "world" #msg.header.frame_id # '%s/fcu' % uavname
    pubState.publish(msgState)
    # rate.sleep()


def handle_uav_pose(msg):
    msg.header.frame_id = "world"
    pub.publish(msg)    
    rate.sleep()
    
if __name__ == '__main__':
    rospy.init_node('uav_pose_listener')
    uavname = rospy.get_param('~uavName')

    newName = uavname
    ex = ey = ez = 0
    RandomErrorCalculated = False
    lastCommandTime = 0
    if uavname =="uav1":
        newName = "SQ01s"
    
    if uavname =="uav2":
        newName = "SQ02s"
    
    if uavname =="uav3":
        newName = "SQ03s"
    
    if uavname =="uav4":
        newName = "SQ04s"
    
    if uavname =="uav5":
        newName = "SQ05s"
    
    if uavname =="uav6":
        newName = "SQ06s"

    pub = rospy.Publisher('/%s/world' % newName, PoseStamped,queue_size=10)
    pubState = rospy.Publisher('/%s/state' % newName, State,queue_size=10)
    rate = rospy.Rate(10) # 10hz


    rospy.Subscriber('/%s/odometry/odom_main' % uavname,
                     Odometry,
                     handle_uav_odom,uavname)

    rospy.Subscriber('/%s/odometry/odom_main' % uavname,
                     Odometry,
                     handle_uav_odom_local,uavname)

    # rospy.Subscriber('/%s/mavros/local_position/pose' % uavname,
    #                  PoseStamped,
    #                  handle_uav_pose)

    rospy.spin()
