# formation control node

## v0.1 (by Ali Ahmadi)
This is a node written in Python to receive data from [MRS UAV system](https://bitbucket.org/semfire-isr-uc/mrs_uav_system), and add error model to the raw position data, then set them for [aclswarm](https://bitbucket.org/semfire-isr-uc/aclswarm/) to calculate the formation. The formation is then fed to [PX4-avoidance](https://bitbucket.org/semfire-isr-uc/px4-avoidance) package.

## Usage
### Simulations

The following command run the position listener for every single drone indicated by **UAV_NAME** variable.
```
export UAV_NAME=uav1; roslaunch formation_control_node uav_pos_listener.launch
```

The following command send the formation commands to the [PX4-avoidance](https://bitbucket.org/semfire-isr-uc/px4-avoidance) for every single drone indicated by **UAV_NAME** variable.
```
export UAV_NAME=uav1; waitForControl; roslaunch formation_control_node formation.launch
```

These commands are used in [this tmux scripts](https://bitbucket.org/semfire-isr-uc/semfire-path-planning/src/master/tmux/formation-control/)